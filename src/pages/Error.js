import {Link} from "react-router-dom"
import { Button } from "react-bootstrap";

// Error Image
import Image from "../images/error.png"


export default function Error() {

    return (
        <>
            <div className="mt-5 text-center">
                <h1>Error 404 - Page Not Found</h1>
                <div className="my-3">
                    <img style={{height: 250}} alt="error-luffy" src={Image} />
                </div>
                <div className="mt-5">
                    <Button size="lg" variant="dark" as={Link} to="/">Back to Home</Button>
                </div>
            </div>
        </>
        
    )
};